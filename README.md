# XWiki Slideshow Macro

This macro allows to display a set of images as a slideshow either inline or in fullscreen using the [SwiperJS](https://swiper.js.com) library.

* Project Lead: [slauriere](http://www.xwiki.org/xwiki/bin/view/XWiki/slauriere)
* [Documentation](https://extensions.xwiki.org/xwiki/bin/view/Extension/Slideshow%20macro/)
* Communication: [Forum / Chat / Mailing List>https://dev.xwiki.org/xwiki/bin/view/Community/Discuss]
* Minimal XWiki version supported: 8.4.10
* License: LGPL 2.1
